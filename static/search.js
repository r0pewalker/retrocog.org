/* global elasticlunr */
function debounce(func, wait) {
  let timeout;

  return function setup(...args) {
    const context = this;
    clearTimeout(timeout);

    timeout = setTimeout(() => {
      timeout = null;
      func.apply(context, args);
    }, wait);
  };
}

// Taken from mdbook
// The strategy is as follows:
// First, assign a value to each word in the document:
//  Words that correspond to search terms (stemmer aware): 40
//  Normal words: 2
//  First word in a sentence: 8
// Then use a sliding window with a constant number of words and count the
// sum of the values of the words within the window. Then use the window that got the
// maximum sum. If there are multiple maximas, then get the last one.
// Enclose the terms in <b>.
function makeTeaser(body, terms) {
  const TERM_WEIGHT = 40;
  const NORMAL_WORD_WEIGHT = 2;
  const FIRST_WORD_WEIGHT = 8;
  const TEASER_MAX_WORDS = 30;

  const stemmedTerms = terms.map((w) => elasticlunr.stemmer(w.toLowerCase()));
  let termFound = false;
  let index = 0;
  const weighted = []; // contains elements of ["word", weight, index_in_document]

  // split in sentences, then words
  const sentences = body.toLowerCase().split('. ');
  // Object.keys(sentences).forEach(i => {
  for (const i in Object.keys(sentences)) {
    const words = sentences[i].split(' ');
    let value = FIRST_WORD_WEIGHT;

    // Object.keys(words).forEach(j => {
    for (const j in Object.keys(words)) {
      const word = words[j];

      if (word.length > 0) {
        // Object.keys(stemmedTerms).forEach(k => {
        for (const k in Object.keys(stemmedTerms)) {
          if (elasticlunr.stemmer(word).startsWith(stemmedTerms[k])) {
            value = TERM_WEIGHT;
            termFound = true;
          }
        }
        weighted.push([word, value, index]);
        value = NORMAL_WORD_WEIGHT;
      }

      index += word.length;
      index += 1; // ' ' or '.' if last word in sentence
    }

    index += 1; // because we split at a two-char boundary '. '
  }

  if (weighted.length === 0) {
    return body;
  }

  const windowWeights = [];
  const windowSize = Math.min(weighted.length, TEASER_MAX_WORDS);
  // We add a window with all the weights first
  let curSum = 0;
  for (let f = 0; f < windowSize; f += 1) {
    curSum += weighted[f][1];
  }
  windowWeights.push(curSum);

  for (let e = 0; e < weighted.length - windowSize; e += 1) {
    curSum -= weighted[e][1];
    curSum += weighted[e + windowSize][1];
    windowWeights.push(curSum);
  }

  // If we didn't find the term, just pick the first window
  let maxSumIndex = 0;
  if (termFound) {
    let maxFound = 0;
    // backwards
    for (let d = windowWeights.length - 1; d >= 0; d -= 1) {
      if (windowWeights[d] > maxFound) {
        maxFound = windowWeights[d];
        maxSumIndex = d;
      }
    }
  }

  const teaser = [];
  let startIndex = weighted[maxSumIndex][2];
  for (let c = maxSumIndex; c < maxSumIndex + windowSize; c += 1) {
    const termWord = weighted[c];
    const stopIndex = 2;
    const stopTermWord = termWord[stopIndex];
    if (startIndex < stopTermWord) {
      // missing text from index to start of `word`
      teaser.push(body.substring(startIndex, stopTermWord));
      startIndex = stopTermWord;
    }

    // add <em/> around search terms
    if (termWord[1] === TERM_WEIGHT) {
      teaser.push('<b>');
    }
    startIndex = termWord[2] + termWord[0].length;
    teaser.push(body.substring(termWord[2], startIndex));

    if (termWord[1] === TERM_WEIGHT) {
      teaser.push('</b>');
    }
  }
  teaser.push('…');
  return teaser.join('');
}

function formatSearchResultItem(item, terms) {
  return '<div class="search-results__item">'
  + `<a href="${item.ref}">${item.doc.title}</a>`
  + `<div>${makeTeaser(item.doc.body, terms)}</div>`
  + '</div>';
}

function initSearch() {
  const $searchInput = document.getElementById('search');
  const $searchResults = document.querySelector('.search-results');
  const $searchResultsItems = document.querySelector('.search-results__items');
  const MAX_ITEMS = 10;

  const options = {
    bool: 'AND',
    fields: {
      title: { boost: 2 },
      body: { boost: 1 },
    },
  };
  let currentTerm = '';
  const index = elasticlunr.Index.load(window.searchIndex);

  $searchInput.addEventListener('keyup', debounce(() => {
    const term = $searchInput.value.trim();
    if (term === currentTerm || !index) {
      return;
    }
    $searchResults.style.display = term === '' ? 'none' : 'block';
    $searchResultsItems.innerHTML = '';
    if (term === '') {
      return;
    }

    const results = index.search(term, options);
    if (results.length === 0) {
      $searchResults.style.display = 'none';
      return;
    }

    currentTerm = term;
    for (let i = 0; i < Math.min(results.length, MAX_ITEMS); i += 1) {
      const item = document.createElement('li');
      item.innerHTML = formatSearchResultItem(results[i], term.split(' '));
      $searchResultsItems.appendChild(item);
    }
  }, 150));
}

if (document.readyState === 'complete'
    || (document.readyState !== 'loading' && !document.documentElement.doScroll)
) {
  initSearch();
} else {
  document.addEventListener('DOMContentLoaded', initSearch);
}
